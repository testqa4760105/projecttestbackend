import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.Integer.parseInt;

public abstract class AbstractTest {
    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String username;
    private static String password;

    private static String token;
    private static int id;
    private static int idPosts;
    private static int idPostsZeroPage;

    private static String usernameShort;
    private static String passwordShort;
    private static String usernameLong;
    private static String passwordLong;
    private static String usernameSpecSymbol;
    private static String passwordSpecSymbol;


    @BeforeAll
    static void initTest() throws IOException {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        username = prop.getProperty("username");
        password = prop.getProperty("password");
        token = prop.getProperty("X-Auth-Token");
        id = parseInt(prop.getProperty("id"));
        idPosts = parseInt(prop.getProperty("idPosts"));
        idPostsZeroPage = parseInt(prop.getProperty("idPostsZeroPage"));
        usernameShort = prop.getProperty("usernameShort");
        passwordShort = prop.getProperty("passwordShort");
        usernameLong = prop.getProperty("usernameLong");
        passwordLong = prop.getProperty("passwordLong");
        usernameSpecSymbol = prop.getProperty("usernameSpecSymbol");
        passwordSpecSymbol = prop.getProperty("passwordSpecSymbol");
    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    public static String getToken() {
        return  token;
    }

    public static int getId() {
        return  id;
    }

    public static int getIdPosts() {
        return  idPosts;
    }
    public static int getIdPostsZeroPage() {
        return  idPostsZeroPage;
    }

    public static String getUsernameShort() {
        return usernameShort;
    }
    public static String getPasswordShort() {
        return passwordShort;
    }

    public static String getUsernameLong() {
        return usernameLong;
    }

    public static String getPasswordLong() {
        return passwordLong;
    }

    public static String getUsernameSpecSymbol() {
        return usernameSpecSymbol;
    }

    public static String getPasswordSpecSymbol() {
        return passwordSpecSymbol;
    }

}
