import api.PostsData;
import api.PostsRoot;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestSuiteGeekPosts extends AbstractTest{
    private static final String URL = "https://test-stand.gb.ru/";
    @Test
    public void postAuth(){

        Specifications.installSpecification(Specifications.requestPostSpec(URL), Specifications.responseSpecCode(200));

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("username", getUsername());
        jsonAsMap.put("password", getPassword());

        Response response = (Response) given()
                .filter(new AllureRestAssured())
                .formParams(jsonAsMap)
                .when()
                .post("gateway/login")
                .then().log().all()
                .extract()
                .body();

        assertThat(response.jsonPath().get("id"), equalTo(getId()));
        assertThat(response.jsonPath().get("username"), equalTo(getUsername()));
        assertThat(response.jsonPath().get("token"), equalTo(getToken()));
        assertThat(response.jsonPath().get("roles"), notNullValue());

    }

    @Test
    public void postNegativeAuthShortLogin(){

        Specifications.installSpecification(Specifications.requestPostSpec(URL), Specifications.responseSpecCode(401));

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("username", getUsernameShort());
        jsonAsMap.put("password", getPasswordShort());

        Response response = (Response) given()
                .filter(new AllureRestAssured())
                .formParams(jsonAsMap)
                .when()
                .post("gateway/login")
                .then()
                .extract()
                .body();
    }

    @Test
    public void postNegativeAuthLongLogin(){

        Specifications.installSpecification(Specifications.requestPostSpec(URL), Specifications.responseSpecCode(401));

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("username", getUsernameLong());
        jsonAsMap.put("password", getPasswordLong());

        Response response = (Response) given()
                .formParams(jsonAsMap)
                .when()
                .post("gateway/login")
                .then()
                .extract()
                .body();
    }

    @Test
    public void postNegativeAuthSpecSymbolLogin(){

        Specifications.installSpecification(Specifications.requestPostSpec(URL), Specifications.responseSpecCode(401));

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("username", getUsernameSpecSymbol());
        jsonAsMap.put("password", getPasswordSpecSymbol());

        Response response = (Response) given()
                .formParams(jsonAsMap)
                .when()
                .post("gateway/login")
                .then()
                .extract()
                .body();
    }

    @Test
    public void postNegativeAuthEmptyLogin(){

        Specifications.installSpecification(Specifications.requestPostSpec(URL), Specifications.responseSpecCode(400));

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("password", getPassword());

        Response response = (Response) given()
                .filter(new AllureRestAssured())
                .formParams(jsonAsMap)
                .when()
                .post("gateway/login")
                .then()
                .extract()
                .body();
    }

    @Test
    public void postNegativeAuthEmptyPassword(){

        Specifications.installSpecification(Specifications.requestPostSpec(URL), Specifications.responseSpecCode(400));

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("username", getUsername());

        Response response = (Response) given()
                .filter(new AllureRestAssured())
                .formParams(jsonAsMap)
                .when()
                .post("gateway/login")
                .then()
                .extract()
                .body();
    }

    @Test
    public void postNegativeFail404(){

        Specifications.installSpecification(Specifications.requestPostSpec(URL), Specifications.responseSpecCode(404));

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("username", getUsername());
        jsonAsMap.put("password", getPassword());

        Response response = (Response) given()
                .formParams(jsonAsMap)
                .when()
                .post("gateway/login/test")
                .then()
                .extract()
                .body();
    }

    @Test
    public void getTheirPosts(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        PostsRoot theirPosts = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?sort=createdAt&order=ASC&page=2")
                .then().log().all()
                .extract().as(PostsRoot.class);
        List<PostsData> data = theirPosts.getData();

        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getAuthorId(), equalTo(getId()));
        }

        int a = getIdPosts();
        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getId(), equalTo(a));
            a++;
        }

        assertThat(data.size(), equalTo(10));

    }

    @Test
    public void getTheirPostsZeroPage(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        PostsRoot theirPosts = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?sort=createdAt&order=ASC&page=0")
                .then().log().all()
                .extract().as(PostsRoot.class);
        List<PostsData> data = theirPosts.getData();

        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getAuthorId(), equalTo(getId()));
        }

        int a = getIdPostsZeroPage();
        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getId(), equalTo(a));
            a++;
        }

        assertThat(data.size(), equalTo(10));

    }

    @Test
    public void getTheirPostsPageNotFound(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());
        int[] a = new int[] {};

        Response response = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?sort=createdAt&order=ASC&page=10000000")
                .then().log().all()
                .body("data", notNullValue())
                .body("meta.prevPage", notNullValue())
                .body("nextPage", equalTo(null))
                .body("meta.count", notNullValue())
                .extract().response();
    }

    @Test
    public void getTheirPostsDesc(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        PostsRoot theirPosts = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?sort=createdAt&order=DESC&page=2")
                .then().log().all()
                .extract().as(PostsRoot.class);
        List<PostsData> data = theirPosts.getData();

        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getAuthorId(), equalTo(getId()));
        }

        int a = data.get(0).getId();
        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getId(), equalTo(a));
            a--;
        }

        assertThat(data.size(), equalTo(10));

    }

    @Test
    public void getTheirPostsNotSort(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        PostsRoot theirPosts = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?order=ASC&page=2")
                .then().log().all()
                .extract().as(PostsRoot.class);
        List<PostsData> data = theirPosts.getData();

        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getAuthorId(), equalTo(getId()));
        }

        int a = getIdPosts();
        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getId(), equalTo(a));
            a++;
        }

        assertThat(data.size(), equalTo(10));

    }

    @Test
    public void getOtherPosts(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        PostsRoot theirPosts = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?owner=notMe&sort=createdAt&order=ASC&page=1")
                .then()
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("JsonSchemaFile.json"))
                .extract().as(PostsRoot.class);
        List<PostsData> data = theirPosts.getData();

        for(int i = 0; i < data.size(); i++){
            int a = getIdPosts();
            assertThat(data.get(i).getAuthorId(), notNullValue());
        }


        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getId(), notNullValue());
        }

        assertThat(data.size(), equalTo(4));

    }

    @Test
    public void getOtherPostsZeroPage(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        PostsRoot theirPosts = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?owner=notMe&sort=createdAt&order=ASC&page=0")
                .then().log().all()
                .extract().as(PostsRoot.class);
        List<PostsData> data = theirPosts.getData();

        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getAuthorId(), notNullValue());
        }


        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getId(), notNullValue());
        }

        assertThat(data.size(), equalTo(4));


    }

    @Test
    public void getOtherPostsPageNotFound(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        Response response = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?owner=notMe&sort=createdAt&order=ASC&page=100000")
                .then().log().all()
                .body("data", notNullValue())
                .body("meta.prevPage", notNullValue())
                .body("nextPage", equalTo(null))
                .body("meta.count", notNullValue())
                .extract().response();
    }

    @Test
    public void getOtherPostsDesc(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        PostsRoot theirPosts = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?owner=notMe&sort=createdAt&order=DESC&page=1")
                .then().log().all()
                .extract().as(PostsRoot.class);
        List<PostsData> data = theirPosts.getData();

        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getAuthorId(), notNullValue());
        }


        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getId(), notNullValue());
        }

        assertThat(data.size(), equalTo(4));

    }

    @Test
    public void getOtherPostsNotSort(){
        Specifications.installSpecification(Specifications.requestGetSpec(URL), Specifications.responseSpec());

        PostsRoot theirPosts = given()
                .when()
                .header("X-Auth-Token", getToken())
                .get("api/posts?owner=notMe&order=ASC&page=1")
                .then().log().all()
                .extract().as(PostsRoot.class);
        List<PostsData> data = theirPosts.getData();

        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getAuthorId(), notNullValue());
        }


        for(int i = 0; i < data.size(); i++){
            assertThat(data.get(i).getId(), notNullValue());
        }

        assertThat(data.size(), equalTo(4));

    }


}
