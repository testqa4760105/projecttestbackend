package api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PostsRoot {
    private List<PostsData> data;
    private PostsMeta meta;
    @JsonCreator
    public PostsRoot(@JsonProperty("data") List<PostsData> data, @JsonProperty("meta") PostsMeta meta) {
        this.data = data;
        this.meta = meta;
    }

    public List<PostsData> getData() {
        return data;
    }

    public PostsMeta getMeta() {
        return meta;
    }
}
