package api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostsMainImage {
    private Object id;
    private String cdnUrl;
    @JsonCreator
    public PostsMainImage(@JsonProperty("id") Object id, @JsonProperty("cdnUrl") String cdnUrl) {
        this.id = id;
        this.cdnUrl = cdnUrl;
    }

    public Object getId() {
        return id;
    }

    public String getCdnUrl() {
        return cdnUrl;
    }
}
