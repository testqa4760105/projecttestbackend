package api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostsMeta {
    private Integer prevPage;
    private Integer nextPage;
    private Integer count;
    @JsonCreator
    public PostsMeta(@JsonProperty("prevPage") Integer prevPage, @JsonProperty("nextPage") Integer nextPage, @JsonProperty("count") Integer count) {
        this.prevPage = prevPage;
        this.nextPage = nextPage;
        this.count = count;
    }

    public Integer getPrevPage() {
        return prevPage;
    }

    public Integer getNextPage() {
        return nextPage;
    }

    public Integer getCount() {
        return count;
    }
}
