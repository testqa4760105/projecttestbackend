package api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
public class PostsData {
    private Integer id;
    private String title;
    private String description;
    private String content;
    private Integer authorId;
    private PostsMainImage mainImage;
    private String updatedAt;
    private String createdAt;
    private ArrayList<Object> labels;
    private String delayPublishTo;
    private Boolean draft;
    @JsonCreator
    public PostsData(@JsonProperty("id") Integer id, @JsonProperty("title") String title, @JsonProperty("description") String description, @JsonProperty("content") String content, @JsonProperty("authorId") Integer authorId, @JsonProperty("mainImage") PostsMainImage mainImage, @JsonProperty("updatedAt") String updatedAt, @JsonProperty("createdAt") String createdAt, @JsonProperty("labels") ArrayList<Object> labels, @JsonProperty("delayPublishTo") String delayPublishTo, @JsonProperty("draft") Boolean draft) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.content = content;
        this.authorId = authorId;
        this.mainImage = mainImage;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.labels = labels;
        this.delayPublishTo = delayPublishTo;
        this.draft = draft;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getContent() {
        return content;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public PostsMainImage getMainImage() {
        return mainImage;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public ArrayList<Object> getLabels() {
        return labels;
    }

    public String getDelayPublishTo() {
        return delayPublishTo;
    }

    public Boolean isDraft() {
        return draft;
    }

}
